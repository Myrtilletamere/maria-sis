
var game = new Phaser.Game(600, 460, Phaser.AUTO, '', {
	preload: preload,
	create: create,
	update: update,
});

var player,
cursors,
platforms,
pipesIn,
pipesOut,
brickPlatforms,
secretBlocks,
hasCollidedWithPipesAlready = false,
skyBackground,
caveBackground,
castleWin,
peachWin,
stateText,
caveCover;

function preload() {
	console.log('preload');
	game.load.image('sky', 'assets/betterSky.png');
	game.load.image('cave', 'assets/cave.png');
	game.load.image('cover', 'assets/cover.jpg');
	game.load.spritesheet('pow', 'assets/dude.png', 32, 48);
	game.load.image('grass', 'assets/betterGrass.png');
	game.load.image('brickBlock', 'assets/brickBlock.png');
	game.load.image('questionBlock', 'assets/questionBlock.png');
	game.load.image('pipe', 'assets/pipe.png');
	game.load.image('castle', 'assets/castle.png');
	game.load.image('peach', 'assets/peach.png');




}

function create() {
	console.log('create');
	game.physics.startSystem(Phaser.Physics.ARCADE);

	game.world.setBounds(0, 0, 2000, 900);

	skyBackground = game.add.tileSprite(0, 0, game.world.width, game.world.height, 'sky');
	game.stage.image = 'sky';
	caveBackground = game.add.tileSprite(0, 450, game.world.width, game.world.height, 'cave');
	game.stage.image = 'cave';
	caveBackground.alpha = 1;
	// caveCover = game.add.tileSprite(0, 912, 500, 30, 'cover');
	// game.stage.image = 'cover';


	player = game.add.sprite(32, game.world.height -800, 'pow');//32
	game.physics.arcade.enable(player);
	player.body.gravity.y=850; //How heavy/high player can jump
	player.body.collideWorldBounds = true;
	player.animations.add('left', [0, 1, 2, 3], 10, true);
	player.animations.add('right', [5, 6, 7, 8], 10, true);
	// game.camera.follow(player);
	game.camera.follow(player, Phaser.Camera.FOLLOW_LOCKON);

	cursors = game.input.keyboard.createCursorKeys();

	platforms = game.add.group();
	platforms.enableBody = true;

	pipesIn = game.add.group();
	pipesIn.enableBody = true;

	brickPlatforms = game.add.group();
	brickPlatforms.enableBody = true;
	brickPlatforms.alpha = 0;

  pipesOut = game.add.group();
	pipesOut.enableBody = true;

	pipesExit = game.add.group();
	pipesExit.enableBody = true;

	secretBlocks = game.add.group();
	secretBlocks.enableBody = true;

	castleWin = game.add.group();
	castleWin.enableBody = true;

	peachWin = game.add.group();
	peachWin.enableBody = true;
	peachWin.alpha = 0;



	//CAVE

  	//Cave walls
  brickBlock(865, 450, 30, 350, false); //first cave wall, behind ground platform
  brickBlock(865, 790, 750, 30, false);
  brickPlatform(1417, 732, 30, 10, false);//cave platforms to get back up
  brickPlatform(1417, 672, 30, 10, false);
  brickPlatform(1417, 600, 30, 10, false);//""
  brickPlatform(1417, 530, 30, 10, false);//""
  brickPlatform(1417, 470, 30, 10, false);//""
  	//Cave Things
  // brickBlock(1300, 672, 30, 30, false);
  secretBlock(1250, 672, 30, 30, false);
  // brickBlock(1360, 672, 30, 30, false);

  	//Exit Pipes
  pipeExit(1330, 735, 70, 55, false);
  pipeExit(1400, 420, 60, 500, false);



  //Upper level
	platform(0, 425, 895, 32 );
	platform(950, 425, 450, 32);
	platform(1460, 425, game.world.width, 32);
	platform(0, game.world.height +300, game.world.width, 32)
	brickBlock(550, 320, 30, 30, false);//first brick
	questionBlock(580, 320, 30, 30, false); //if true, moves when in contact with player
	brickBlock(610, 320, 30, 30, false);//second brick

	pipeIn(900, 375, 60, 55, false);//pipe going down
	brickBlock(1300, 250, 30, 180, false);//wall
	brickPlatform(1417, 400, 30, 10, false);//last platform inside 'out' pipe
	pipeOut(1400, 375, 60, 55, false);//pipe going up

	castle(1600, 125, 200, 310, false);
	peach(1650, 200, 50, 125, false);


	// game.add.sprite(0, 455, 'cave');

	 stateText = game.add.text( 1500 , 180,' ', { font: '40px Arial', fill: '#992d2d' });
   stateText.anchor.setTo(0.5, 0.5);
   stateText.visible = false;

}

function platform(x, y, width, height, movable) {
	var ledge = new Phaser.TileSprite(game, x, y, width, height, 'grass');
	platforms.add(ledge);
	ledge.body.immovable = !movable; //makes it a variable to change platforms
	// ledge.body.collideWorldBounds = true;

}
function brickPlatform(x, y, width, height, movable) {
	var ledge = new Phaser.TileSprite(game, x, y, width, height, 'brickBlock');
	brickPlatforms.add(ledge);
	ledge.body.immovable = !movable; //makes it a variable to change platforms
	ledge.body.collideWorldBounds = true;
	ledge.body.checkCollision.down = false;
	ledge.body.checkCollision.up = true;
}

function brickBlock(x, y, width, height, movable) {
	var ledge = new Phaser.TileSprite(game, x, y, width, height, 'brickBlock');
	platforms.add(ledge);
	ledge.body.immovable = !movable; //makes it a variable to change platforms

}
function questionBlock(x, y, width, height, movable) {
	var ledge = new Phaser.TileSprite(game, x, y, width, height, 'questionBlock');
	platforms.add(ledge);
	ledge.body.immovable = !movable; //makes it a variable to change platforms
}

function secretBlock(x, y, width, height, movable) {
	var ledge = new Phaser.TileSprite(game, x, y, width, height, 'questionBlock');
	secretBlocks.add(ledge);
	ledge.body.immovable = !movable; //makes it a variable to change platforms
	ledge.body.collideWorldBounds = true;

}
function pipeIn(x, y, width, height, movable) {
	var ledge = new Phaser.TileSprite(game, x, y, width, height, 'pipe');
	pipesIn.add(ledge);
	ledge.body.immovable = !movable; //makes it a variable to change platforms
	ledge.body.collideWorldBounds = true;
	ledge.body.checkCollision.down = false;
	ledge.body.checkCollision.up = true;
}
function pipeOut(x, y, width, height, movable) {
	var ledge = new Phaser.TileSprite(game, x, y, width, height, 'pipe');
	pipesOut.add(ledge);
	ledge.body.immovable = !movable; //makes it a variable to change platforms
	ledge.body.collideWorldBounds = true;
	ledge.body.checkCollision.down = false;
	ledge.body.checkCollision.up = true;
}
function castle(x, y, width, height, movable) {
	var ledge = new Phaser.TileSprite(game, x, y, width, height, 'castle');
	castleWin.add(ledge);
	ledge.body.immovable = !movable; //makes it a variable to change platforms
	ledge.body.collideWorldBounds = true;
}

function peach(x, y, width, height, movable) {
	var ledge = new Phaser.TileSprite(game, x, y, width, height, 'peach');
	peachWin.add(ledge);
	ledge.body.immovable = !movable; //makes it a variable to change platforms
	// ledge.body.collideWorldBounds = true;

}

function pipeExit(x, y, width, height, movable) {
	var ledge = new Phaser.TileSprite(game, x, y, width, height, 'cover');
	pipesExit.add(ledge);
	ledge.body.immovable = !movable; //makes it a variable to change platforms
	ledge.body.collideWorldBounds = false;

}



var playerSpeed = 200;
var jumpSpeed = 300;
var hasJumped = false;
var jumpButtonReleased = false;

function update(){


	game.physics.arcade.collide(player, platforms);
	game.physics.arcade.collide(player, pipesIn, collidingWithPipes, null, this);
	game.physics.arcade.collide(player, pipesOut);
	game.physics.arcade.collide(player, brickPlatforms);
	game.physics.arcade.collide(player, secretBlocks, collidingWithSecretBlock, null, this);
  game.physics.arcade.collide(player, pipesExit/*, collidingWithPipeExit, null, this*/);
  game.physics.arcade.collide(player, castleWin, collidingWithCastle, null, this);


	player.body.velocity.x=0;
	// console.log(pipe);
	// console.log(secretBlocks);

	if (cursors.right.isDown){
		player.body.velocity.x= playerSpeed;
		player.animations.play('right');
	} else if (cursors.left.isDown){
		player.body.velocity.x = -playerSpeed;
		player.animations.play('left');
	} else {
		player.animations.stop();
		player.frame =4;
	}

	if (cursors.up.isDown && player.body.touching.down)
	{
		player.body.velocity.y = -350;
	}



    function collidingWithPipes(player, pipesIn)
    {
    	console.log('collision between maria and pipe');
    	// The two sprites are colliding
    	if (player.body.touching.down && player.body.touching.left == false && player.body.touching.right == false && cursors.down.isDown && hasCollidedWithPipesAlready == false)
    	{
    		// game.stage.backgroundColor = '#992d2d';
			console.log(game.stage);
    		stopColliding();

      }
  	}

  	function collidingWithSecretBlock(player, secretBlocks)
    {
    	console.log('collision between maria and secret block');

    	// The two sprites are colliding
    		// game.stage.backgroundColor = '#992d2d';
    		// stopColliding();
    		// caveCover.alpha = 0;
    		brickPlatforms.alpha = 1;
    		pipesExit.destroy();






      }
  	}

 		function collidingWithCastle(player, castleWin){
 			console.log('collide with castle');

 			stateText.text = " Thank you Maria! \n You saved me!\n Have a puzzle piece!";
 			stateText.visible = true;
 			game.input.onTap.addOnce(close,this);

 			peachWin.alpha = 1;
 			}

  	function stopColliding(){

  		hasCollidedWithPipesAlready = true;

			caveBackground.alpha = 0;
  		skyBackground.alpha = 1;
  		pipesIn.destroy();









}
